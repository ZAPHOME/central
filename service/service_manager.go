package service

import (
	"bitbucket.org/zaphome/sharedapi/context"
	"bitbucket.org/zaphome/sharedapi/logging"
	"bitbucket.org/zaphome/central/configuration"
)

var CentralContext *context.Context = nil

func InitializeCentralContext() *context.Context {
	CentralContext = context.CreateEmptyContext()
	logger := logging.CreateConsoleLogger(configuration.CENTRAL_SERVICE_NAME)
	CentralContext.AddServiceWithTypeName(*logger)
	return CentralContext
}
