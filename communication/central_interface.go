package communication

import (
	"bitbucket.org/zaphome/central/service"
	"bitbucket.org/zaphome/sharedapi/logging"
	"bitbucket.org/zaphome/sharedapi/messaging"
	"bitbucket.org/zaphome/sharedapi/messaging/communication"
)

const CENTRAL_CHANNEL_ID int = 0

var (
	logger         logging.Logger
	CentralChannel *communication.ObservableMessageChannel
)

func InitializeCentralInterface() {
	// Logging
	logger = (*service.CentralContext.GetService("Logger")).(logging.Logger)
	defer logger.Info("Initialized central interface")

	// Create central channel
	CentralChannel = communication.GetNewObservableMessageChannelFromChannel(make(chan messaging.Message))

	// Define observers
	CentralChannel.AddGlobalObserverFunction(processMessage)

	// Start the observing of the central channel
	CentralChannel.StartObserving()
}

func processMessage(message messaging.Message) {
	logger.Debug("Received message on central channel: %+v", message)
}


