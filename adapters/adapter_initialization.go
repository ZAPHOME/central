package adapters

import (
	"sync"
	"plugin"
	"bitbucket.org/zaphome/sharedapi/adapters"
	"bitbucket.org/zaphome/sharedapi/messaging/communication"
	centralCommunication "bitbucket.org/zaphome/central/communication"
)

var (
	nextAdapterId      int        = 0
	nextAdapterIdMutex sync.Mutex = sync.Mutex{}
)

type InitializeFunction func(adapter adapters.Adapter) adapters.Adapter

type AdapterInitializationError struct {
	Message string
}

func (e AdapterInitializationError) Error() string {
	return e.Message
}

func InitializeAdapterPlugin(p *plugin.Plugin, name string) (*adapters.Adapter, error) {
	adapter := getAdapterForPlugin(name)
	initFunction, err := loadInitFunction(p)
	if err != nil {
		return nil, AdapterInitializationError{
			"Could not trigger the adapters initialization. This could be a problem with the adapters. " +
				"Please make sure that the adapters version is compatible with this central version.",
		}
	} else {
		initializedAdapter := initFunction(*adapter)
		return &initializedAdapter, nil
	}
}

func getAdapterForPlugin(name string) *adapters.Adapter {
	nextAdapterIdMutex.Lock()
	defer nextAdapterIdMutex.Unlock()
	nextAdapterId++

	return &adapters.Adapter{
		Id:         nextAdapterId,
		Name:       name,
		MessageBus: communication.GetNewMessageBusWithSource(centralCommunication.CentralChannel),
	}
}

func loadInitFunction(plugin *plugin.Plugin) (InitializeFunction, error) {
	initSymbol, err := plugin.Lookup(initFunction)
	if err != nil {
		return nil, err
	}
	initFunction, ok := initSymbol.(func(adapter adapters.Adapter) adapters.Adapter)
	if ok {
		return initFunction, nil
	} else {
		return nil, AdapterInitializationError{"Could not trigger the adapters initialization."}
	}
}
