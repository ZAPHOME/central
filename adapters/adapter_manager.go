package adapters

import (
	"os"
	"io/ioutil"
	"plugin"
	"path/filepath"
	"bitbucket.org/zaphome/central/service"
	"bitbucket.org/zaphome/sharedapi/logging"
	"strings"
	"unicode"
	"bitbucket.org/zaphome/sharedapi/adapters"
	"bitbucket.org/zaphome/sharedapi/messaging"
	"bitbucket.org/zaphome/central/communication"
	"reflect"
)

const adapterDirectory = "adapter"
const initFunction = "Initialize"

var logger logging.Logger
var loadedAdapters []*adapters.Adapter

func GetAllLoadedAdapters() []adapters.Adapter {
	loadedAdapts := make([]adapters.Adapter, 0)
	for _, adapt := range loadedAdapters {
		loadedAdapts = append(loadedAdapts, *adapt)
	}
	return loadedAdapts
}

func FindAdapterById(id int) *adapters.Adapter {
	for _, adapter := range loadedAdapters {
		if adapter.Id == id {
			return adapter
		}
	}
	return nil
}

func InitializeAllAdapters() {
	logger = (*service.CentralContext.GetService("Logger")).(logging.Logger)
	loadedAdapters = loadAllAdapters()
	logger.Info("Loaded adapters: %+v", GetAllLoadedAdapters())
	communication.CentralChannel.AddObserverFunction(
		reflect.TypeOf(&adapters.AdapterListRequest{}),
		processAdapterListMessage,
	)
	communication.CentralChannel.AddGlobalObserverFunction(processAdapterMessage)
}

func loadAllAdapters() []*adapters.Adapter {
	c := make(chan *adapters.Adapter)
	defer close(c)
	currentAdapters := make([]*adapters.Adapter, 0)
	for _, file := range getAllPluginFiles() {
		go loadPlugin(file, c)
		adapter := <- c
		if adapter != nil && adapter.Id != -1 {
			currentAdapters = append(currentAdapters, adapter)
		}
	}
	return currentAdapters
}

func getAllPluginFiles() []os.FileInfo {
	files, err := ioutil.ReadDir(getAdapterPath())
	if err != nil {
		logger.Panic("Could not load adapters: %v", err)
	}
	return files
}

func getAdapterPath() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		logger.Panic("Could not load adapters: %v", err)
	}
	return filepath.Join(dir, adapterDirectory)
}

func openPluginFromFile(info *os.FileInfo) (*plugin.Plugin, error) {
	path := filepath.Join(getAdapterPath(), (*info).Name())
	return plugin.Open(path)
}

func loadPlugin(file os.FileInfo, c chan *adapters.Adapter) {
	// Load the plugin
	p, err := openPluginFromFile(&file)
	if err != nil {
		logger.Error("Cannot load adapters (%s): %v", file.Name(), err)
		c <- nil
		return
	}

	// Initialize the plugin
	adapter, err := InitializeAdapterPlugin(p, buildAdapterName(file.Name()))
	if err != nil {
		logger.Error("Cannot load adapters (%s): %v", file.Name(), err)
		c <- nil
	} else {
		c <- adapter
	}
}

// TODO Refactoring. Create util function for naming conventions
func buildAdapterName(pluginName string) string {
	// Remove 'adapters.so' at the end like: z_wave_test_adapter.so --> z_wave_test
	pluginName = strings.Split(pluginName, ".")[0]
	if strings.HasSuffix(pluginName, "adapters") {
		pluginName = pluginName[:len(pluginName) - len("adapters")]
	}
	if strings.HasSuffix(pluginName, "_") {
		pluginName = pluginName[:len(pluginName) - len("_")]
	}

	// Replace underscores to spaces z_wave_test --> z wave test
	pluginName = strings.Replace(pluginName, "_", " ", -1)

	// Write the first letter before an space
	// or at the start in upper case z wave test --> Z Wave Test
	var input = []rune(pluginName)
	var result []rune
	for i := 0; i < len(input); i++ {
		if (i == 0) || (input[i] == ' ') && (len(input) > i + 1) {
			result = append(result, unicode.ToUpper(input[i]))
		} else {
			result = append(result, input[i])
		}
	}

	return string(result)
}

func processAdapterListMessage(message messaging.Message) {
	// Build the response based on the request
	request := message.(*adapters.AdapterListRequest)
	response := adapters.BuildAdapterListResponse(communication.CENTRAL_CHANNEL_ID,
			request.GetId(), GetAllLoadedAdapters())

	// Send back the response
	adapter := FindAdapterById(request.GetSourceId())
	adapter.MessageBus.DestinationChannel.SendMessage(response)
}

func processAdapterMessage(message messaging.Message) {
	// If the message is for a specific adapter and not the central, ...
	if instruction, ok := message.(messaging.Instruction);
			ok && instruction.GetDestinationId() != communication.CENTRAL_CHANNEL_ID {
		// ... redirect the message
		targetAdapter := FindAdapterById(instruction.GetDestinationId())
		targetAdapter.MessageBus.DestinationChannel.SendMessage(message)
	}
}
