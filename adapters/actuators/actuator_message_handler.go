package actuators

import (
	"reflect"
	"bitbucket.org/zaphome/sharedapi/adapters/actuators"
	"bitbucket.org/zaphome/central/communication"
	"bitbucket.org/zaphome/sharedapi/messaging"
	"bitbucket.org/zaphome/central/adapters"
	"bitbucket.org/zaphome/sharedapi/adapters/actuators/states"
)

func Initialize() {
	communication.CentralChannel.AddObserverFunction(
		reflect.TypeOf(&actuators.ActuatorListRequest{}),
		handleActuatorRequest,
	)
	communication.CentralChannel.AddObserverFunction(
		reflect.TypeOf(&actuators.ActuatorConnectedEvent{}),
		handleAddActuatorMessage,
	)
	communication.CentralChannel.AddObserverFunction(
		reflect.TypeOf(&states.ActuatorStateChangedEvent{}),
		handleActuatorStateChangedEvent,
	)
}

func handleActuatorRequest(message messaging.Message) {
	// Build the response based on the request
	request := message.(*actuators.ActuatorListRequest)
	response := actuators.BuildActuatorListResponse(communication.CENTRAL_CHANNEL_ID,
		request.GetId(), GetActuators())

	// Send back the response
	adapter := adapters.FindAdapterById(request.GetSourceId())
	adapter.MessageBus.DestinationChannel.SendMessage(response)
}

func handleAddActuatorMessage(message messaging.Message) {
	request := message.(*actuators.ActuatorConnectedEvent)
	AddActuator(&request.Actuator)
}

func handleActuatorStateChangedEvent(message messaging.Message) {
	event := message.(*states.ActuatorStateChangedEvent)
	actuator := FindActuatorById(event.ActuatorId)
	actuator.State = event.State
}
