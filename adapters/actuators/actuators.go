package actuators

import (
	"sync"

	"bitbucket.org/zaphome/sharedapi/adapters/actuators"
)

var (
	loadedActuators []*actuators.Actuator
	nextId          int
	nextIdMutex     sync.Mutex
)

func GetActuators() []actuators.Actuator {
	loadedActuats := make([]actuators.Actuator, 0)
	for _, actuat := range loadedActuators {
		loadedActuats = append(loadedActuats, *actuat)
	}
	return loadedActuats
}

func FindActuatorById(id int) *actuators.Actuator {
	for _, actuator := range loadedActuators {
		if actuator.Id == id {
			return actuator
		}
	}
	return nil
}

func AddActuator(actuator *actuators.Actuator) {
	nextIdMutex.Lock()
	defer nextIdMutex.Unlock()
	nextId++

	actuator.Id = nextId
	loadedActuators = append(loadedActuators, actuator)
}
