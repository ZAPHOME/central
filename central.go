package main

import (
	"bufio"
	"os"
	"strings"

	"bitbucket.org/zaphome/central/adapters"
	"bitbucket.org/zaphome/central/adapters/actuators"
	"bitbucket.org/zaphome/central/communication"
	"bitbucket.org/zaphome/central/service"
	sharedApiAdapters "bitbucket.org/zaphome/sharedapi/adapters"
	"bitbucket.org/zaphome/sharedapi/logging"
)

var logger logging.Logger

func main() {
	ctx := service.InitializeCentralContext()
	logger = (*ctx.GetService("Logger")).(logging.Logger)

	logger.Info("Initializing ZapHome Central service...")
	initCentral()
	initAdapters()
	logger.Info("Central service initialized successfully")

	// Blocks the main thread
	StartCli()
}

func initCentral() {
	communication.InitializeCentralInterface()
	actuators.Initialize()
}

func initAdapters() {
	adapters.InitializeAllAdapters()
}

func StartCli() {
	for true {
		reader := bufio.NewReader(os.Stdin)
		text, _ := reader.ReadString('\n')
		text = strings.TrimRight(text, "\n")
		handleCommand(text)
	}
}

func handleCommand(command string) {
	switch command {
	case "":
		// Do nothing
	case "stop", "exit", "q":
		Shutdown()
	case "healthcheck", "hc":
		HealthCheck()
	case "status", "s":
		logger.Info("Loaded adapters: %+v", adapters.GetAllLoadedAdapters())
	case "help", "h", "?":
		Help()
	default:
		logger.Warn("Unknown command: %s (type 'h' for help)", command)
	}
}

func HealthCheck() {
	logger.Info("Executing a health check on all initialized adapters...")
	for _, adapter := range adapters.GetAllLoadedAdapters() {
		_, err := adapter.MessageBus.SendRequestToDestinationAndWaitForResponse(
			sharedApiAdapters.BuildAdapterHealthCheckRequest(communication.CENTRAL_CHANNEL_ID, adapter.Id),
		)
		if err != nil {
			logger.Error("HealthCheck error on adapter with id: %d (%v)", adapter.Id, err)
		} else {
			logger.Info("HealthCheck done for adapter with id: %d", adapter.Id)
		}
	}
}

func Help() {
	logger.Info("Help (command[, alias...] description):\n%s\n%s\n%s\n%s",
		"stop, exit, q\tShutdown Zaphome",
		"healthcheck, hc\tExecute a heath check on all loaded adapters",
		"status, s\tShow the server status",
		"help, h, ?\tDisplay this help text")
}

func Shutdown() {
	logger.Info("Shutting down...")
	os.Exit(0)
}
